const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}

function totalearnings(favouritesMovies) {
    const keys = Object.keys(favouritesMovies).reduce((acc,Movies)=>{ 
        if(parseInt(favouritesMovies[Movies].totalEarnings.slice(1,-1))>500)
         acc[Movies] = favouritesMovies[Movies];
         return acc;

    },{});
    return keys;
    
}
//console.log(totalearnings(favouritesMovies));

function oscarnominationsandtotalEarnings(favouritesMovies) {
    const keys = Object.keys(favouritesMovies).reduce((acc,Movies)=>{ 
        if((favouritesMovies[Movies]. oscarNominations>3 && favouritesMovies[Movies].totalEarnings.slice(1,-1))>500)
         acc[Movies] = favouritesMovies[Movies];
         return acc;
    },{});
    return keys;
}
//console.log(oscarnominationsandtotalEarnings(favouritesMovies));

function actorofMovies(favouritesMovies) {
    let keys = Object.keys(favouritesMovies).reduce((acc, Movies)=> {
        if(favouritesMovies[Movies].actors.includes("Leonardo Dicaprio")){
        acc[Movies] = favouritesMovies[Movies];
        }
        return acc;
        
    },{});
       return keys;
     }
// console.log(actorofMovies(favouritesMovies));

function sortMovies(favouritesMovies){
    const sortedMovies = Object.fromEntries(
        Object.entries(favouritesMovies).sort(([,a],[,b])=>{
            if( a.imdbRating>b.imdbRating)
            return 1;

            else if(a.imdbRating<b.imdbRating)
            return -1;

            else 
  
            if((parseInt(a.totalEarnings.slice(1,-1))> parseInt(b.totalEarnings.slice(1,-1))))
            return 1;

            else if((parseInt(a.totalEarnings.slice(1,-1))< parseInt(b.totalEarnings.slice(1,-1))))
            return -1;

            else
            return 0;
        } )
         )
           return sortedMovies;
        }
        //console.log(sortMovies(favouritesMovies));
    


